module main;

import crossemu.sdk.n64;
import crossemu.gdk.n64.loz_oot;

import std.stdio : writeln;

mixin(initPlugin!(
    "Breath of Time",
    "This mod adds mechanics from 'The Legend of Zelda : Breath of the Wild' " ~
    "to Ocarina of Time, such as stamina, wall climbing, running, and jumping.",
    0x01_00_00_00, // 1.0.0.0,
    false
));

private // local variables
{
    RdRam ddBorder;

    Button btnJump = Button.A;
    Button btnStamina = Button.L;

    float curStamina = 0f;
    int multStamina = 20;
    int timeRegen = 0;
    int timeWall = 0;
    bool jumpNeedAdjust = false;
    bool jumpPressed = false;
    bool jumpModeActive = true;
    bool staminaDepleted = false;
    bool swapQueued = false;
}

bool initialize()
{
    // make sure gdk loads
    if (!gdkInitialize())
        return false;
    
    // make sure we are using the expected version
    if (currentVersion != GameVersion.NTSC_1_0)
        return false;
    
    ddBorder = address[currentVersion][Address.Save] + 0xCFU;

    // everything checked out
    return true;
}

void terminate()
{
    gdkTerminate();
}

void onFirstFrame()
{
	gdkOnFirstFrame();
}

void onTick(uint frame)
{
    // process any gdk tick stuff first
    gdkOnTick(frame);

    // don't do weird stuff on title/load screens
    if (!player.exists) return;

    // read 'button' codes
    auto btnState = global.buttonState;

    // do the actual stamina based checks
    handleStamina(!handleClimbing() && !handleRunning());

    // // do the non-z-target jump
    handleJumpSwap(btnState);
    handleJumping(btnState);
}

void handleJumpSwap(uint btnState)
{
    auto isPressed = (btnState & cast(uint) Button.DUp) != 0;

    // checks for single press only
    if (isPressed)
    {
        if (!swapQueued) 
            swapQueued = true;
    }
    else
    {
        if (!swapQueued) return;

        // change mode variable
        jumpModeActive = !jumpModeActive;

        // set z-target handlers
        if (jumpModeActive)
        {
            rdram.write!ushort(0x3AA5AEU, 0x2A60U);
            rdram.write!ushort(0x3928B0U, 0x4C1U);
            rdram.write!ubyte(0x3928D3U, 0x1U);
        }
        else
        {
            rdram.write!ushort(0x3928B0U, 0x1CC0U);
            rdram.write!ubyte(0x3928D3U, 0x2U);
        }

        swapQueued = false;
    }
}

bool handleClimbing()
{
    // not climbing
    if (!player.isState(PlayerState.Climbing))
    {
        timeWall = 0;
        return false;
    }

    if (staminaDepleted) timeWall += 1;
    if (!staminaDepleted && curStamina > 0 /* canClimb */)
    {
        // wall slide timer
        if (timeWall < 8)
            timeWall += 1;

        if (!(player.moved) /* useStamina */)
        {
            // only take stamina when we move
            curStamina += 1;
        }
        else
        {
            // only double speed after attaching to wall
            if (timeWall < 8) return true;

            if (!global.isButtonPressed(btnStamina)) return true;

            // we are moving, check if need double speed
            player.multiplyPosition(1.01f, 1.01f, 1.01f);
            curStamina -= 0.5f;
        }
    }
    else
    {
        if (timeWall < 8) return true;

        /* Perform down slide */
        player.resetPosition(true, true, true);
        player.posY = player.posY - 4;
    }

    return true;
}

bool handleRunning()
{
    // wait for full recharge
    if (staminaDepleted) return false;

    // escape if not holding run button
    if (!global.isButtonPressed(btnStamina)) return false;

    // make sure we are in a valid state to run
    if (
        player.isState(
            PlayerState.Busy, PlayerState.Climbing, PlayerState.Damaged, PlayerState.Dying,
            PlayerState.Falling, PlayerState.Shielding, PlayerState.Talking,
            PlayerState.Transforming,
            PlayerState.ChargingSword, PlayerState.ClimbingLedge, PlayerState.FirstPerson,
            PlayerState.GettingItem, PlayerState.RidingEpona, PlayerState.SceneTransition,
            PlayerState.UseItem, PlayerState.DisabledFloorCollision, PlayerState.HangingFromLedge,
            PlayerState.ClimbingOutOfWater,
            PlayerState.Crawling, PlayerState.Diving, PlayerState.Horizontal,
            PlayerState.Idle1, PlayerState.Idle2, PlayerState.Ocarina, PlayerState.Shopping,
            PlayerState.CanCrawl, PlayerState.CanRead, PlayerState.MoveSword,
            PlayerState.UnderWater, PlayerState.ConnectedToEnemy
        )
    ) return false;

    // make sure we have stamina to use
    if (curStamina < 1) return false;

    // make sure we are moving
    if (!player.moved) return false;

    // add half speed quarter gravity extra to normal speed
    player.posX = player.posX + player.velX / 2;
    player.posY = player.posY + player.velY / 4;
    player.posZ = player.posZ + player.velZ / 2;

    return true;
}

void handleStamina(bool canRegen)
{
    auto maxStamina = save.heartContainers * multStamina;

    if (canRegen)
    {
        /* stamina goes up */
        if (timeRegen < 30) timeRegen += 1;
        else
        {
            if (curStamina < maxStamina) curStamina += 2;
            if (curStamina > maxStamina) curStamina = maxStamina;
            if (curStamina == maxStamina) staminaDepleted = false;
        }
    }
    else
    {
        /* stamina goes down */
        timeRegen = 0;
        curStamina -= 1;

        if (curStamina < 0) curStamina = 0;
        if (curStamina == 0) staminaDepleted = true;
    }

    // display stamina
    import std.math : floor;
    ddBorder << (cast(ubyte) floor(curStamina / multStamina));
}

void handleJumping(uint btnState)
{
    // make sure we are using jump logic
    if (!jumpModeActive) return;

    if (jumpNeedAdjust)
    {
        // adjust velocity second frame of jump
        player.velY = 5;

        // set jump state
        player.setState(PlayerState.Jumping);

        jumpNeedAdjust = false;
    }

    // escape if not pressing jump button
    if ((btnState & cast(uint) btnJump) == 0)
    {
        jumpPressed = false;
        return;
    }

    // make sure we are pressing jump this frame and moving
    if (jumpPressed) return;

    // make sure we arent interacting with options
    if (!runtime.isInteractState(InteractState.Attack,
        InteractState.Blank, InteractState.PutAway)) return;

    // make sure we are in a valid state to jump
    auto animId = player.animationID;
    if (
        player.isState(
            PlayerState.Busy, PlayerState.Climbing, PlayerState.Damaged, PlayerState.Dying,
            PlayerState.Falling, PlayerState.Jumping, PlayerState.Swimming, PlayerState.Talking,
            PlayerState.Transforming, PlayerState.ChargingSword, PlayerState.ClimbingLedge,
            PlayerState.FirstPerson, PlayerState.GettingItem, PlayerState.HoldingActor,
            PlayerState.RidingEpona, PlayerState.SceneTransition, PlayerState.UseItem,
            PlayerState.DisabledFloorCollision, PlayerState.HangingFromLedge,
            PlayerState.HoldingRangedWeapon, PlayerState.ClimbingOutOfWater,
            PlayerState.CanCrawl, PlayerState.Crawling, PlayerState.Diving,
            PlayerState.Horizontal, PlayerState.Idle1, PlayerState.Idle2, PlayerState.Ocarina,
            PlayerState.Shopping, PlayerState.CanRead, PlayerState.MoveSword,
            PlayerState.UnderWater, PlayerState.ConnectedToEnemy
        ) ||
        animId == 0x3240U ||
        animId == 0x3170U ||
        animId == 0x3020U ||
        animId == 0x3040U ||
        !player.moving
    ) return;

    // add some instant up positioning
    if (player.elevating) player.posY = player.posY + 13.5f;
    else if (player.lowering) player.posY = player.posY + 7.5f;
    else player.posY = player.posY + 10;

    // add upward velocity
    player.velY = 8;

    jumpPressed = true;
    jumpNeedAdjust = true;
}